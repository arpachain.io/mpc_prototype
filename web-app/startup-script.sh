rm /home/{username}/scale/Scripts/logs/*
echo "abc" > /tmp/mpc_started
gsutil cp /tmp/mpc_started gs://{project_id}.appspot.com/{request_id}/mpc_started
sudo runuser -l root -c "cd /home/{username}/scale && ./Scripts/test.sh {program} &> /tmp/mpc_full_output"
sed -i '/{username}/d' /home/{username}/scale/Scripts/logs/*
sed -i 1,18d /home/{username}/scale/Scripts/logs/0
sed -i 1,18d /home/{username}/scale/Scripts/logs/1
sed -i 1,18d /home/{username}/scale/Scripts/logs/2
# Post the MPC result to GCS so that it can be displayed on frontend.
gsutil cp -r /home/{username}/scale/Scripts/logs/* gs://{project_id}.appspot.com/{request_id}
gsutil cp /tmp/mpc_full_output gs://{project_id}.appspot.com/{request_id}/mpc_full_output
# Shut down the GCE VM.
sudo shutdown -h now
