# MPC POC website 

This is a flask implemented web interface for automatically running example programs in
SCALE-MAMBA MPC framework.

The app is tested with Google App Engine Standard.
Before running or deploying this application, install and configure gcloud following the
 instructions [here](https://cloud.google.com/sdk/docs/quickstart-linux)
Then enter virtualenv and install the dependencies using
[pip](http://pip.readthedocs.io/en/stable/):

    virtualenv env
    source env/bin/activate
    pip install -t lib -r requirements.txt

To deploy the app on Google App Engine:

    gcloud app deploy

