import logging
import os

from flask import Flask, render_template, request, jsonify
import uuid
import cloudstorage
from google.appengine.api import app_identity
import googleapiclient.discovery
import googleapiclient.discovery

app = Flask(__name__)

bucketName = app_identity.get_default_gcs_bucket_name()

INSTANCE_ZONE = "us-east1-b"
PROJECT = "arpa-mpc"
IMAGE_NAME = "scale-image2"
SNAPSHOT_NAME = "scale-stable-3"
VM_USERNAME="shaolong_chen"

# Compute Engine service stub.
compute = googleapiclient.discovery.build('compute', 'v1')


@app.route('/')
def index():
    path = os.path.dirname(__file__) + '/mpc_programs'
    files = [f.replace('.mpc', '') for f in os.listdir(path)]
    return render_template('index.html', files=files, program_content=read_demo_file(files[0]))


def create_instance(project, zone, uid, bucket, program):
    instance_name = "mpc-" + uid
    image_response = compute.images().get(
        project=PROJECT, image=IMAGE_NAME).execute()
    source_disk_image = image_response['selfLink']


    # Configure the machine
    machine_type = "zones/%s/machineTypes/n1-standard-1" % zone
    startup_script = open(
            os.path.join(os.path.dirname(__file__), 'startup-script.sh'), 'r').read()
    startup_script = startup_script.format(project_id=PROJECT, program=program, request_id=uid, username=VM_USERNAME)

    config = {
        'name': instance_name,
        'machineType': machine_type,

        # Specify the boot disk and the image to use as a source.
        'disks': [
            {
                'boot': True,
                'autoDelete': True,
                'initializeParams': {
                    'sourceImage': source_disk_image,
                }
            }
        ],

        # Specify a network interface with NAT to access the public
        # internet.
        'networkInterfaces': [{
            'network': 'global/networks/default',
            'accessConfigs': [
                {'type': 'ONE_TO_ONE_NAT', 'name': 'External NAT'}
            ]
        }],

        # Allow the instance to access cloud storage and logging.
        'serviceAccounts': [{
            'email': 'default',
            'scopes': [
                'https://www.googleapis.com/auth/devstorage.read_write',
                'https://www.googleapis.com/auth/logging.write'
            ]
        }],

        # Metadata is readable from the instance and allows you to
        # pass configuration from deployment scripts to instances.
        'metadata': {
            'items': [{
                # Startup script is automatically executed by the
                # instance upon startup.
                'key': 'startup-script',
                'value': startup_script
            }, {
                'key': 'bucket',
                'value': bucket
            }]
        }
    }

    return compute.instances().insert(
        project=project,
        zone=zone,
        body=config).execute()


@app.route('/run', methods=['POST'])
def run_mpc():
    program = request.form['file']

    request_id = str(uuid.uuid4())
    logging.info("request_id: " + request_id)

    result = create_instance(PROJECT, INSTANCE_ZONE, request_id, bucket=bucketName, program=program)
    logging.info(result)

    return render_template('setup_mpc.html', request_id=request_id, program=program)


@app.route('/start_mpc/<request_id>')
def start_mpc(request_id):
    return render_template('result.html', request_id=request_id)


@app.route('/check_result')
def result():
    request_id = request.args['request_id']
    dir_name = "/{0}/{1}/".format(bucketName, request_id)
    # By default we run mpc with 3 nodes, each has its own log file, and a file
    # named test_output that contains the computation result.
    files = [ "0", "1", "2", "test_output" ]

    try:
        gcs_file = cloudstorage.open(dir_name + "mpc_full_output")
        content = str(gcs_file.read())
        gcs_file.close()
        logging.info("Full mpc result: " + content)
        # The existence of mpc_full_output file indicates that all result files
        # have been uploaded. So that we can safely display results.
        results = {}
        for file_name in files:
            try:
                gcs_file = cloudstorage.open(dir_name + file_name)
                content = str(gcs_file.read())
                # logging.info("file_name: " + file_name + ", content: " + content)
                gcs_file.close()
                results[file_name] = content
            except cloudstorage.NotFoundError:
                pass
        return jsonify(result=results)
    except cloudstorage.NotFoundError:
        return jsonify()


@app.route('/delete_instance')
def delete_instance():
    request_id = request.args['request_id']
    instance_name = "mpc-" + request_id
    disk_name = "disk-" + request_id
    instance_resp = compute.instances().delete(
        project=PROJECT,
        zone=INSTANCE_ZONE,
        instance=instance_name).execute()
    return jsonify(result=instance_resp)


@app.route('/check_setup_status')
def setup_status():
    request_id = request.args['request_id']
    dir_name = "/{0}/{1}/".format(bucketName, request_id)
    try:
        gcs_file = cloudstorage.open(dir_name + "mpc_started")
        content = str(gcs_file.read())
        gcs_file.close()
        return jsonify(result="DONE")
    except cloudstorage.NotFoundError:
        return jsonify(result="PENDING")


@app.route('/demo_file/<file_name>')
def demo_file(file_name):
    return jsonify(result=read_demo_file(file_name))


@app.errorhandler(500)
def server_error(e):
    # Log the error and stacktrace.
    logging.exception('An error occurred during a request.')
    return 'An internal error occurred.', 500


def read_demo_file(file_name):
    path = os.path.dirname(__file__) + '/mpc_programs'
    with open('{0}/{1}.mpc'.format(path, file_name), 'r') as f:
        program_content = f.read()
    return program_content

